from django.shortcuts import render
from rest_framework import viewsets
from .models import Employee
from core.serialize import EmployeeSerializer

class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
